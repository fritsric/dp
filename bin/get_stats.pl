#!/usr/bin/perl

use File::Basename;
use bignum;

if (scalar(@ARGV) != 2)
{
    die "Usage: get_stats.pl data_dir 24|12|4";
}

$dir = $ARGV[0];
$cores = $ARGV[1];
@inputs = glob("${dir}/*");
#print join("\n", @inputs) . "\n";
mkdir "${dir}_csv";

$|++;

@proc_counts;

if ($cores == 12)
{
    @proc_counts = (2, 4, 8, 12, 24, 36, 48, 60, 72);
}
elsif ($cores == 4)
{
    @proc_counts = (2, 4, 8, 12, 16, 20, 24);
}
elsif ($cores == 24)
{
    @proc_counts = (2, 4, 8, 12, 24, 36, 48);
}
else
{
    die "specify 24, 12 or 4 cores";
}

sub replace_in_file
{
    my $file = $_[0];
    my $old = $_[1];
    my $new = $_[2];
    
    open(my $fh, "<", $file) or die "Cannot open $file: $!";
    my @lines = <$fh>;
    close($fh);
    
    open($fh, ">", $file) or die "Cannot open $file: $!";

    foreach(@lines)
    {
        $_ =~ s/$old/$new/;
        print $fh $_;
    }

    close($fh);
}

sub run
{
    my $o = `$_[0]`;
    $o =~ m/\D+ (\d+) \D+/;
    #print "pid: " . $1 . "\n";
    return $1;
}

sub read_stats
{
    my $pid = $_[0];
    while (`qstat -u $ENV{"USER"}`)
    {
        sleep 1;
    }
    my @e_logs = glob("*l_job.sh.e$pid");
    #print join("\n", @e_logs) . "\n";
    foreach $l (@e_logs)
    {
        if (!(-z $l))
        {
            #`qdel -u $ENV{"USER"}`;
            print "Errors in $l\n";
            return (-1, -1);
        }
    }
    
    my @o_logs = glob("*l_job.sh.o$pid");
    #print join("\n", @o_logs) . "\n";

    foreach $l (@o_logs)
    {
        open(my $input_fh, "<", $l) or die "Cannot open $input_file: $!";
        my @lines = <$input_fh>;
        close $input_fh;
        $lines[1] =~ m/(\d+\.\d+)s/;
        my $t = $1;
        $lines[2] =~ m/(\d+\.\d+)GB/;
        my $m = $1;
        unlink glob ("*l_job.sh.pe$pid *l_job.sh.po$pid *l_job.sh.o$pid *l_job.sh.e$pid");
        return ($t, $m);
    }
}



foreach $in (@inputs)
{
    my $serial_time;
    my $serial_mem;
    foreach $infi (0..1)
    {
        my $net = $infi ? "infiniband" : "ethernet";
        open(my $out, ">", "${dir}_csv/" . basename($in) . "_" . $cores . "c_" . $net . ".csv") or die $!;
        print $out "n, procs, time, speedup, efficiency, memory\n";
        my $t;
        my $m;
        if ($infi == 0)
        {
            replace_in_file('serial_job.sh', '\#\$ -q \d+c_serial.q', '#$ -q '.$cores.'c_serial.q');
            replace_in_file('serial_job.sh', 'fme_seq \S+ ', 'fme_seq '.$in.' ');
            my $pid = run('qsub serial_job.sh');
            print "serial ($in): $pid, t = ";
            ($t, $m) = ($serial_time, $serial_mem) = read_stats($pid);
            print "$t s\n";
        }
        else
        {
            ($t, $m) = ($serial_time, $serial_mem);
        }
        
        print $out "1, 1, $t, 1, 1, $m\n"; 
        my $i = 2;
        foreach $procs (@proc_counts)
        {
            replace_in_file('parallel_job.sh', 'fme_mpi \S+ ', 'fme_mpi '.$in.' ');
            replace_in_file('parallel_job.sh', 'INFINIBAND="\S+"', 'INFINIBAND="'.($infi ? 'true' : 'false').'"');    
            my $pid = run("qsub -pe ompi $procs -q ${cores}c_vip.q parallel_job.sh");
            print "parallel $procs $net ($in): $pid, t = ";
            ($t, $m) = read_stats($pid);
            print "$t s\n";
            my $speedup = $serial_time / $t;
            my $eff = $speedup / $procs;
            print $out "$i, $procs, $t, $speedup, $eff, $m\n";
            $i++;
        }
    }
}

