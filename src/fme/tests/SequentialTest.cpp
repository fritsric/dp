#include <sstream>
#include <numeric>

#include "cppunit/extensions/HelperMacros.h"

#include <gmpxx.h>

#undef PUT_RANK
#undef LOG_LEVEL
#define LOG_LEVEL ERROR_LEVEL
#include "Logging.h"
#include "SequentialInequalitySolver.h"

using namespace std;

template <typename T>
class SequentialTest : public CPPUNIT_NS::TestFixture
{
    typedef SequentialInequalitySolver<T> IS;
public:
    void setUp()
    {
        istringstream ss("7 3\n"
                "0 -1 -3 < -10\n"
                "0 0 -1 < -1\n"
                "-1 0 6 < -1\n"
                "0 1 3 < 15\n"
                "0 0 1 < 3\n"
                "0 0 0 < 0\n"
                "1 0 -6 < 50");
        is.Read(ss);
    }

    void tearDown()
    {

    }

    void TestRead()
    {
        CPPUNIT_ASSERT(is.t_orig.size() == 7);
        CPPUNIT_ASSERT(is.t_orig[0].size() == 4);
        CPPUNIT_ASSERT(is.t_orig[3].size() == 4);
        CPPUNIT_ASSERT(is.t_orig[6].size() == 4);
        CPPUNIT_ASSERT(is.t_orig[2][3] == 6);

        istringstream ss("3 2\n"
                        "3 -2 < 7\n"
                        "1 3 < 17\n"
                        "4 1 > 13");
        is.Read(ss);
        CPPUNIT_ASSERT(is.t_orig.size() == 3);
        CPPUNIT_ASSERT(is.t_orig[2].size() == 3);
        CPPUNIT_ASSERT(is.t_orig[1][0] == 17);
        CPPUNIT_ASSERT(is.t_orig[2][1] == -4);
    }

    void TestSort()
    {
        const typename IS::size_t col1 = 3;

        CPPUNIT_ASSERT(is.t_plus.size() == 3);
        CPPUNIT_ASSERT(is.t_minus.size() == 3);
        CPPUNIT_ASSERT(is.t_zero.size() == 1);
        CPPUNIT_ASSERT(is.t_zero[0][col1] == 0);

        istringstream ss("3 2\n"
            "3 -2 < 7\n"
            "1 3 < 17\n"
            "4 1 > 13");
        is.Read(ss);
        const typename IS::size_t col2 = 2;
        CPPUNIT_ASSERT(is.t_plus.size() == 1);
        CPPUNIT_ASSERT(is.t_minus.size() == 2);

        ss.clear();
        ss.str("3 2\n"
            "4 0 < 13\n"
            "3 -2 < 7\n"
            "1 -1 < 17"
            );
        is.Read(ss);
        CPPUNIT_ASSERT(is.t_plus.empty());
        CPPUNIT_ASSERT(is.t_zero.size() == 1);
        CPPUNIT_ASSERT(is.t_minus.size() == 2);

        ss.clear();
        ss.str("3 2\n"
            "4 0 < 13\n"
            "3 2 < 7\n"
            "1 1 < 17"
            );
        is.Read(ss);
        CPPUNIT_ASSERT(is.t_plus.size() == 2);
        CPPUNIT_ASSERT(is.t_zero[0][col2] == 0);
        CPPUNIT_ASSERT(is.t_minus.empty());

        ss.clear();
        ss.str("5 2\n"
            "1 2 < 7\n"
            "2 3 < 17\n"
            "3 1 < 13\n"
            "4 2 < 7\n"
            "5 3 < 17");
        is.Read(ss);
        CPPUNIT_ASSERT(is.t_plus.size() == 5);
        CPPUNIT_ASSERT(is.t_minus.empty());

        ss.clear();
        ss.str("5 2\n"
            "11 3 < 17\n"
            "12 1 < 13\n"
            "13 -2 < 7\n"
            "14 3 < 17\n"
            "15 1 < 13");
        is.Read(ss);
        CPPUNIT_ASSERT(is.t_plus.size() == 4);
        CPPUNIT_ASSERT(is.t_minus.size() == 1);
        CPPUNIT_ASSERT(is.t_zero.empty());
    }

    void TestFindRanges() {}

    void TestFindOneSolution() {}


    CPPUNIT_TEST_SUITE(SequentialTest);

    CPPUNIT_TEST(TestRead);
    CPPUNIT_TEST(TestSort);
    CPPUNIT_TEST(TestFindRanges);
    CPPUNIT_TEST(TestFindOneSolution);

    CPPUNIT_TEST_SUITE_END();

private:
    IS is;

};

template<>
void SequentialTest<mpq_class>::TestFindRanges()
{
    istringstream ss("3 2\n"
            "3 -2 < 7\n"
            "1 3 < 17\n"
            "4 1 > 13");
    is.Read(ss);
    typename IS::Result lb, ub;
    CPPUNIT_ASSERT(is.Solve(lb, ub));
    CPPUNIT_ASSERT(lb[0] == 2);
    CPPUNIT_ASSERT(lb[1] == 1);
    CPPUNIT_ASSERT(ub[0] == 5);
    CPPUNIT_ASSERT(ub[1] == 5);
}

template<>
void SequentialTest<float>::TestFindRanges()
{
    istringstream ss("3 2\n"
            "3 -2 < 7\n"
            "1 3 < 17\n"
            "4 1 > 13");
    is.Read(ss);
    typename IS::Result lb, ub;
    CPPUNIT_ASSERT(is.Solve(lb, ub));
    CPPUNIT_ASSERT(lb[0] == 2);
    CPPUNIT_ASSERT(lb[1] == 1);
    CPPUNIT_ASSERT(ub[0] == 5);
    CPPUNIT_ASSERT(ub[1] == 5);
}

template<>
void SequentialTest<float>::TestFindOneSolution()
{
    istringstream ss("3 2\n"
            "3 -2 < 7\n"
            "1 3 < 17\n"
            "4 1 > 13");
    is.Read(ss);
    typename IS::Result r;
    CPPUNIT_ASSERT(is.Solve(r));
    CPPUNIT_ASSERT(EqualFloats(r[0], 3.5f));
    CPPUNIT_ASSERT(EqualFloats(r[1], 3.125f));
    for (const auto& e : is.t_orig)
    {
        float x = std::inner_product(std::begin(e) + 1, std::end(e), std::begin(r), 0.0f);
        CPPUNIT_ASSERT(x < e[0]);
    }
}

template<>
void SequentialTest<mpq_class>::TestFindOneSolution()
{
    istringstream ss("3 2\n"
            "3 -2 < 7\n"
            "1 3 < 17\n"
            "4 1 > 13");
    is.Read(ss);
    typename IS::Result r;
    CPPUNIT_ASSERT(is.Solve(r));
    CPPUNIT_ASSERT(r[0] == mpq_class(7, 2));
    CPPUNIT_ASSERT(r[1] == mpq_class(25, 8));
    for (const auto& e : is.t_orig)
    {
        mpq_class x = std::inner_product(std::begin(e) + 1, std::end(e), std::begin(r), mpq_class(0));
        CPPUNIT_ASSERT(x < e[0]);
    }
}

CPPUNIT_TEST_SUITE_REGISTRATION(SequentialTest<int>);
CPPUNIT_TEST_SUITE_REGISTRATION(SequentialTest<float>);
CPPUNIT_TEST_SUITE_REGISTRATION(SequentialTest<mpq_class>);
