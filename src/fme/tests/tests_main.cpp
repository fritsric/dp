#include "cppunit/ui/text/TextTestRunner.h"
#include "cppunit/extensions/TestFactoryRegistry.h"

int main()
{
    CppUnit::TextTestRunner runner;
    CppUnit::TestFactoryRegistry &registry = CppUnit::TestFactoryRegistry::getRegistry();
    runner.addTest(registry.makeTest());
    return !runner.run();
}
