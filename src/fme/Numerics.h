#ifndef NUMERICS_H_
#define NUMERICS_H_

#include <cstddef>

#include <gmpxx.h>

#include "include/gtest-internal.h"

typedef std::ptrdiff_t int_t;

/**
 * Method to arbitrarily choose a value from interval, possibly with infinities.
 * @param lb_inf whether lower bound is -inf
 * @param ub_inf whether upper bound is +inf
 * @param lb exact lower bound if not -inf
 * @param ub exact upper bound if not +inf
 * @return the chosen value
 */
template<typename T>
inline T ChooseFromInterval(bool lb_inf, bool ub_inf, const T& lb, const T& ub)
{
    if (lb_inf && ub_inf)
        return T(0);
    if (ub_inf)
        return lb + (lb < T(0) ? T(-lb) : lb);
    if (lb_inf)
        return ub - (ub < T(0) ? T(-ub) : ub);
    return (ub + lb) / T(2);
}

template<class Container, class Element>
inline void move_back(Container& c, Element& e)
{
    c.push_back(std::move(e));
}

template<template<typename> class C, typename T>
std::ostream& operator<<(std::ostream& os, const C<T>& e)
{
    for (size_t i = 1; i < e.size(); ++i)
        os << e[i] << ' ';
    os << "< " << e[0];
    return os;
}


/**
 * A method for correct comparison of floating point values using the GoogleTest method with ULPs
 */
template<typename T>
inline bool EqualFloats(T a, T b)
{
    const FloatingPoint<T> lhs(a), rhs(b);
    return lhs.AlmostEquals(rhs);
}

/**
 * Templated compare against zero, specialised for floats below
 */
template<typename T>
inline bool IsZero(T a)
{
    return a == 0;
}

template<>
inline bool IsZero(double a)
{
    return EqualFloats(a, 0.0);
}

template<>
inline bool IsZero(float a)
{
    return EqualFloats(a, 0.0f);
}

/**
 * Finds the nearest integer less than or equal to x
 */
template<typename T>
inline int_t Floor(const T& x)
{
    return static_cast<int_t>(std::floor(static_cast<double>(x)));
}

/**
 * Finds the nearest integer greater than or equal to x
 */
template<typename T>
inline int_t Ceiling(const T& x)
{
    return static_cast<int_t>(std::ceil(static_cast<double>(x)));
}

template<>
inline int_t Floor(const int_t& x)
{
    return x;
}


template<>
inline int_t Ceiling(const int_t& x)
{
    return x;
}

template<>
inline int_t Floor(const mpq_class& x)
{
    return static_cast<int_t>(std::floor(x.get_d()));
}

template<>
inline int_t Ceiling(const mpq_class& x)
{
    return static_cast<int_t>(std::ceil(x.get_d()));
}

template<>
inline int_t Floor(const mpz_class& x)
{
    return static_cast<int_t>(x.get_si());
}

template<>
inline int_t Ceiling(const mpz_class& x)
{
    return static_cast<int_t>(x.get_si());
}

template<>
inline int_t Floor(const mpf_class& x)
{
    return static_cast<int_t>(floor(x).get_val().get_si());
}

template<>
inline int_t Ceiling(const mpf_class& x)
{
    return static_cast<int_t>(ceil(x).get_val().get_si());
}

#endif /* NUMERICS_H_ */
