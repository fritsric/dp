#ifndef LOGGING_H_
#define LOGGING_H_

/**
 * @file Logging.h
 * Defines logging macros with the following levels:
 * NO_LOGGING
 * CRITICAL_ERROR
 * ERROR - possibly recoverable error
 * WARNING - wrong parameters, wrong configuration
 * INFO - information for the user
 * DEBUG - detailed information about current state
 * TRACE - tracing of each step
 *
 * As the output goes to a stream, you should be able to use the following:
 * CRITICAL_ERROR("x = " << x << y << z);
 *
 * You can define the macros LOG_LEVEL, LOG_STREAM, LOG_LINE_ENDING and PUT_LEVEL_TO_LOG_MSGS e.g. as
 * #define LOG_LEVEL WARNING_LEVEL
 * #define LOG_STREAM std::cerr
 * #define LOG_LINE_ENDING '\n'
 * #define PUT_LEVEL_TO_LOG_MSGS 0
 *
 * By default LOG_LEVEL is set to INFO_LEVEL, LOG_STREAM to std::cout
 * LOG_LINE_ENDING to std::endl and PUT_LEVEL_TO_LOG_MSGS to 1
 *
 * By default, the log messages do not contain the MPI rank of processes.
 * It is always better to have separate log file for every MPI process/task
 * than to have one big and possibly strangely interleaved log. Separate logs
 * can be obtained by passing a parameter --output-filename <filename> to
 * mpirun, which creates filename.rank for every process. The parameter
 * --timestamp-output can also be useful. Or you can open your own ofstream with
 * rank in the filename. If one big combined log is desired, the parameter
 * --tag-output will automatically put rank to each message.
 */

#ifndef LOG_LEVEL
#   define LOG_LEVEL INFO_LEVEL
#endif

#ifndef LOG_STREAM
#   define LOG_STREAM std::cout
#endif

#ifndef LOG_LINE_ENDING
#   define LOG_LINE_ENDING std::endl
#endif

#ifndef PUT_LEVEL
#   define PUT_LEVEL 1
#endif

#ifndef PUT_RANK
#   define PUT_RANK 0
#endif

#if PUT_RANK == 1 && !defined(LOG_RANK)
#   define LOG_RANK mpi::comm::world.rank()
//#   define LOG_RANK rank
#   define LOG_RANK_STREAM << LOG_RANK << ": "
#else
#   define LOG_RANK
#   define LOG_RANK_STREAM
#endif

#define NO_LOGGING_LEVEL     0
#define CRITICAL_ERROR_LEVEL 1
#define ERROR_LEVEL          2
#define WARNING_LEVEL        3
#define INFO_LEVEL           4
#define DEBUG_LEVEL          5
#define TRACE_LEVEL          6

#if PUT_LEVEL == 1
#   define CRITICAL_LEVEL_TEXT "CRITICAL_ERROR: " <<
#   define ERROR_LEVEL_TEXT    "ERROR: " <<
#   define WARNING_LEVEL_TEXT  "WARNING: " <<
#   define INFO_LEVEL_TEXT     "INFO: " <<
#   define DEBUG_LEVEL_TEXT    "DEBUG: " <<
#   define TRACE_LEVEL_TEXT    "TRACE: " <<
#else
#   define CRITICAL_LEVEL_TEXT
#   define ERROR_LEVEL_TEXT
#   define WARNING_LEVEL_TEXT
#   define INFO_LEVEL_TEXT
#   define DEBUG_LEVEL_TEXT
#   define TRACE_LEVEL_TEXT
#endif

#define SEMICOLON_ENDING(code) do { code } while (0)
#define SEND_TO_LOG_STREAM(msg) SEMICOLON_ENDING(LOG_STREAM LOG_RANK_STREAM << msg << LOG_LINE_ENDING;)

#define CRITICAL_ERROR(msg)
#define ERROR(msg)
#define WARNING(msg)
#define INFO(msg)
#define DEBUG(msg)
#define DEBUG_CODE(code)
#define TRACE(msg)
#define TRACE_ROOT(msg)
#define TRACE_CODE(code)

#if LOG_LEVEL > 0
#   undef CRITICAL_ERROR
#   define CRITICAL_ERROR(msg) SEND_TO_LOG_STREAM(CRITICAL_LEVEL_TEXT msg)
#endif
#if LOG_LEVEL > 1
#   undef ERROR
#   define ERROR(msg) SEND_TO_LOG_STREAM(ERROR_LEVEL_TEXT msg)
#endif
#if LOG_LEVEL > 2
#   undef WARNING
#   define WARNING(msg) SEND_TO_LOG_STREAM(WARNING_LEVEL_TEXT msg)
#endif
#if LOG_LEVEL > 3
#   undef INFO
#   define INFO(msg) SEND_TO_LOG_STREAM(INFO_LEVEL_TEXT msg)
#endif
#if LOG_LEVEL > 4
#   undef DEBUG
#   undef DEBUG_CODE
#   define DEBUG(msg) SEND_TO_LOG_STREAM(DEBUG_LEVEL_TEXT msg)
#   define DEBUG_CODE(code) SEMICOLON_ENDING(code)
#endif
#if LOG_LEVEL > 5
#   undef TRACE
#   undef TRACE_ROOT
#   undef TRACE_CODE
#   define TRACE(msg) SEND_TO_LOG_STREAM(TRACE_LEVEL_TEXT msg)
#   define TRACE_ROOT(msg) SEMICOLON_ENDING(if (LOG_RANK == 0) { SEND_TO_LOG_STREAM(TRACE_LEVEL_TEXT msg); })
#   define TRACE_CODE(code) SEMICOLON_ENDING(code)
#endif

#define VAR_ENCLOSE(v) "[" v << "]"
#define DUMP_VAR1(v) #v " = " << v
#define NEXT_VAR(v) << " | " DUMP_VAR1(v)
#define VAR1(v1) VAR_ENCLOSE(DUMP_VAR1(v1))
#define VAR2(v1, v2) VAR_ENCLOSE(DUMP_VAR1(v1) NEXT_VAR(v2))
#define VAR3(v1, v2, v3) VAR_ENCLOSE(DUMP_VAR1(v1) NEXT_VAR(v2) NEXT_VAR(v3))
#define VAR4(v1, v2, v3, v4) VAR_ENCLOSE(DUMP_VAR1(v1) NEXT_VAR(v2) NEXT_VAR(v3) NEXT_VAR(v4))
#define VAR5(v1, v2, v3, v4, v5) VAR_ENCLOSE(DUMP_VAR1(v1) NEXT_VAR(v2) NEXT_VAR(v3) NEXT_VAR(v4) NEXT_VAR(v5))

#define GET_MACRO(_1, _2, _3, _4, _5, NAME, ...) NAME
#define VARS(...) GET_MACRO(__VA_ARGS__, VAR5, VAR4, VAR3, VAR2, VAR1) (__VA_ARGS__)

#endif /* LOGGING_H_ */
