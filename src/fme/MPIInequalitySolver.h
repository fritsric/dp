#ifndef MPIINEQUALITYSOLVER_H_
#define MPIINEQUALITYSOLVER_H_

#include <memory>

#include "SequentialInequalitySolver.h"

#include <mpi.h>
#include "mpp.h"

using namespace mpi;

namespace mpi
{

/**
 * Type traits for std::valarray for the MPP interface to MPI
 */
template <class T>
struct mpi_type_traits<std::valarray<T>>
{
    typedef T element_type;
    typedef T* element_addr_type;

    static inline size_t get_size(std::valarray<T>& vec)
    {
        return vec.size();
    }

    static inline MPI_Datatype get_type(std::valarray<T>&& vec)
    {
        return mpi_type_traits<T>::get_type( T() );
    }

    static inline element_addr_type get_addr(std::valarray<T>& vec)
    {
        return std::begin(vec);
    }
};

}

/**
 * Forward declaration of the unit test of this class
 */
template <typename U>
class MPITest;

/**
 * The MPIInequalitySolver class is a class representing a parallel distributed
 * memory solver of systems of linear inequalities using MPI.
 */
template <typename T>
class MPIInequalitySolver : public SequentialInequalitySolver<T>
{
public:

    /**
     * Constructor, assigns necessary constants.
     */
    MPIInequalitySolver();

    /**
     * Virtual destructor necessary because of virtual methods
     */
    virtual ~MPIInequalitySolver() {}

    /**
     * A method for reading in textual representations of a system of linear inequalities.
     */
    virtual void Read(std::istream& is);

protected:
    typedef SequentialInequalitySolver<T> Base;     /**< typedef for calling the base class */

    //@{
    /**
     * Template classes need this because these types might actually be members
     * of the template parameter.
     */
    using typename Base::size_t;
    using typename Base::Equation;
    using typename Base::Equations;
    using typename Base::Bounds;
    using typename Base::IntEquation;
    //@}

    //@{
    /**
     * I have to say what member functions I want to use from the base class as well.
     */
    using Base::DoRead;
    using Base::PlaceInequality;
    //@}

    /**
     * Gathers and records inequality counts from all nodes and calls the actual redistribution
     * routine for each inequality type.
     * @param new_eq_size
     */
    virtual void RedistributeInequalities(size_t new_eq_size);

    /**
     * Simple enum to differentiate between types of inequalities, can also be used
     * as offsets for global inequality counts.
     */
    enum InequalityType
    {
        Plus = 0,
        Minus = 1,
        Zero = 2
    };

    /**
     * Distributes equations among all the processors - each builds a stack of send or receive operations
     * based on how many equations each processor is missing or has excess of. The stack of operations is
     * then executed (it is a no-operation for nodes with ideal equation counts). To avoid needless
     * communication the nodes send equations only if they have more than one extra.
     * @param new_eq_size size of incoming inequalities
     * @param type the type (+, - or 0) of inequalities to redistribute
     */
    void EquidistributeCategory(size_t new_eq_size, InequalityType type);

    /**
     * Gathers whether arbitrary solution occurred in other nodes and accordingly throws an exception.
     * @param arbitrary_solution whether the solution can be arbitrary in this node
     */
    virtual void CheckArbitrarySolution(bool arbitrary_solution) const;

    /**
     * Gathers whether no solution occurred in other nodes and accordingly throws an exception.
     * @param no_solution indicates non-existence of a solution in this node
     */
    virtual void CheckNoSolution(bool no_solution) const;

    /**
     * Broadcasts inequality categories (governed by the data distribution strategy) to all nodes.
     * @param new_eq_size the size of incoming inequalities
     */
    virtual void BroadcastCategories(size_t new_eq_size);

    /**
     * Performs an all-to-all broadcast of a category of inequalities to all nodes.
     * @param t which category to send from or receive to
     * @param type inequality type
     * @param new_eq_size the size of incoming inequalities
     */
    void BroadcastCategory(Equations& t, InequalityType type, size_t new_eq_size);

    /**
     * Does all-to-all broadcast of bounds and whether they are infinite and upon
     * return the global values are assigned to parameters.
     * @param lb[in|out] lower bound
     * @param ub[in|out] upper bound
     * @param lb_inf[in|out] whether lower bound is -inf
     * @param ub_inf[in|out] whether upper bound is +inf
     */
    virtual void GatherBounds(T& lb, T& ub, bool& lb_inf, bool& ub_inf) const;

    /**
     * Gathers counts of inequalities from all nodes.
     */
    void GatherInequalityCounts();

    /**
     * A method called when std::bad_alloc is caught, terminates the MPI program.
     */
    virtual void OutOfMemory();

    /**
     * A method call when an unexpected exception is caught, terminates the MPI program.
     * @param e the exception
     */
    virtual void UnknownException(const std::exception& e);

    //@{
    /**
     * Template class idiocy again - need to say using for every base member I want to use
     */
    using Base::t_plus;
    using Base::t_minus;
    using Base::t_zero;
    using Base::lower_bounds;
    using Base::upper_bounds;
    using Base::unknowns;
    using Base::t_orig;
    //@}

    const int rank;                            /**< MPI rank of the process */
    const int procs;                           /**< Number of MPI processes/tasks */
    /**
     * array of inequality count triples gathered from all nodes
     */
    const std::unique_ptr<size_t[]> global_counts;
    size_t global_count_plus;                  /**< global count of positive type inequalities */
    size_t global_count_minus;                 /**< global count of negative type inequalities */
    size_t global_count_zero;                  /**< global count of zero type inequalities */

    /**
     * Make the unit test of this class a friend to be able to see private members
     */
    template <typename U>
    friend class MPITest;
};

template<typename T>
MPIInequalitySolver<T>::MPIInequalitySolver()
: Base(),
  rank(comm::world.rank()),
  procs(comm::world.size()),
  global_counts(new size_t[3 * procs]),
  global_count_plus(0),
  global_count_minus(0),
  global_count_zero(0)
{ }

/**
 * A shortcut function for MPI_Datatype deduction
 * @return the MPI_Datatype corresponding to T
 */
template<typename T>
MPI_Datatype MPIType()
{
    return mpi_type_traits<T>::get_type(T());
}

template<typename T>
inline void MPIInequalitySolver<T>::Read(std::istream& is)
{
    int per_proc, remainder;
    size_t n, m;

    // clear all inequality containers
    t_plus.clear();
    t_minus.clear();
    t_zero.clear();
    t_orig.clear();

    if (rank == 0)
    {
        // only the root process actually reads
        DoRead(is);
        // everything is in t_orig
        n = t_orig.size();
        m = unknowns;
        size_t to_send[] = {n, m};
        // broadcast ineq count and number of unknowns
        MPI_Bcast(to_send, 2, MPIType<size_t>(), 0, MPI_COMM_WORLD);
        DEBUG("Bcast " VARS(n, m));

        Equations t(t_orig);


        per_proc = n / procs;
        remainder = n % procs;

        // keep some ineqs
        size_t i;
        for (i = 0; i < per_proc + (remainder ? 1 : 0); ++i)
        {
            PlaceInequality(t[i], m);
        }

        // distribute ineqs to other processes
        for (size_t p = 1; p < procs; ++p)
        {
            for (size_t j = 0; j < per_proc + (p < remainder ? 1 : 0); ++j)
            {
                comm::world(p) << t[i++];
            }
        }

        DEBUG("kept " << t_plus.size() + t_minus.size() + t_zero.size() << " inequalities");
        TRACE(DebugPrint());
    }
    else
    {
        size_t to_recv[2] = {};
        // receive n, m
        MPI_Bcast(to_recv, 2, MPIType<size_t>(), 0, MPI_COMM_WORLD);
        n = to_recv[0];
        unknowns = m = to_recv[1];
        DEBUG("Bcast " VARS(n, m));
        // receive the ineqs
        per_proc = n / procs;
        remainder = n % procs;
        size_t count = per_proc + (rank < remainder ? 1 : 0);
        for (size_t i = 0; i < count; ++i)
        {
            Equation e(m + 1);
            comm::world(0) >> e;
            PlaceInequality(e, m);
        }
        DEBUG("got " << count << " inequalities");
        TRACE(DebugPrint());
    }

    // clear bounds and make space for new
    lower_bounds.clear();
    upper_bounds.clear();

    lower_bounds.reserve(unknowns);
    upper_bounds.reserve(unknowns);
}

template<typename T>
inline void MPIInequalitySolver<T>::GatherInequalityCounts()
{
    size_t loc_n[] = {t_plus.size(), t_minus.size(), t_zero.size()};
    // every process gets the counts of all processes
    MPI_Allgather(loc_n, 3, MPIType<size_t>(), &global_counts[0], 3, MPIType<size_t>(), MPI_COMM_WORLD);
    DEBUG_CODE(
        if (rank == 0)
        {
            std::ostringstream os;
            for (size_t i = 0; i < 3 * procs; i += 3)
            {
                os << "\n  #" << i/3 << ": +: " << global_counts[i] << " | -: " << global_counts[i + 1]
                   << " | 0: " << global_counts[i + 2];
            }
            DEBUG(os.str());
        }
    );

    // sum them up for category sizes
    global_count_plus = 0;
    global_count_minus = 0;
    global_count_zero = 0;
    for (size_t i = 0; i < 3 * procs; i += 3)
    {
        global_count_plus += global_counts[i];
        global_count_minus += global_counts[i + 1];
        global_count_zero += global_counts[i + 2];
    }


    DEBUG_CODE(
        const size_t ideal_plus = global_count_plus / procs;
        const size_t ideal_minus = global_count_minus / procs;
        const size_t ideal_zero = global_count_zero / procs;

        DEBUG(VARS(ideal_plus, ideal_minus, ideal_zero));

        static std::vector<int> diffs_plus(procs);
        static std::vector<int> diffs_minus(procs);
        static std::vector<int> diffs_zero(procs);
        for (size_t i = 0; i < 3 * procs; i += 3)
        {
            diffs_plus[i/3] = global_counts[i] - ideal_plus;
            diffs_minus[i/3] = global_counts[i + 1] - ideal_minus;
            diffs_zero[i/3] = global_counts[i + 2] - ideal_zero;
        }

        TRACE(VARS(diffs_plus[rank], diffs_minus[rank], diffs_zero[rank]));
    );
}

template<typename T>
inline void MPIInequalitySolver<T>::RedistributeInequalities(size_t new_eq_size)
{
    GatherInequalityCounts();

    EquidistributeCategory(new_eq_size, Plus);
    EquidistributeCategory(new_eq_size, Minus);
    EquidistributeCategory(new_eq_size, Zero);

    DEBUG_CODE(
        if (rank == 0)
        {
            std::ostringstream os;
            for (size_t i = 0; i < 3 * procs; i += 3)
            {
                os << "\n  #" << i/3 << ": +: " << global_counts[i] << " | -: " << global_counts[i + 1]
                   << " | 0: " << global_counts[i + 2];
            }
            DEBUG(os.str());
        }
    );
}

template<typename T>
inline void MPIInequalitySolver<T>::EquidistributeCategory(size_t new_eq_size, InequalityType type)
{
    // local class to represent who sends how many ineqs to whom
    struct sendrecv
    {
        int rank;
        int num_eqs;
        bool operator<(const sendrecv& other) const
        {
            return num_eqs < other.num_eqs;
        }
    };

    // gets reused in subsequent calls
    static std::vector<sendrecv> senders, receivers;

    const size_t offset = type;
    bool all_have_eqs = true;
    // quick check if everyone has some ineqs
    for (size_t p = 0; p < procs; ++p)
    {
        if (global_counts[3 * p + offset] == 0)
        {
            all_have_eqs = false;
            break;
        }
    }

    Equations * p = nullptr;
    size_t ideal_count = 0;
    // assign the category container and ideal counts
    switch (type)
    {
    case Plus:
        ideal_count = global_count_plus;
        p = &t_plus;
        break;
    case Minus:
        ideal_count = global_count_minus;
        p = &t_minus;
        break;
    case Zero:
        ideal_count = global_count_zero;
        p = &t_zero;
        break;
    default:
        CRITICAL_ERROR("unknown inequality type " << type << " in EquidistributeCategory");
        break;
    }

    ideal_count /= procs;
    Equations& t = *p;

    // find out how many are extra or missing in all procs
    for (int p = 0; p < procs; ++p)
    {
        const int ideal_diff = global_counts[3 * p + offset] - ideal_count;
        if (ideal_diff > int(all_have_eqs))
        {
            TRACE("sender " VARS(p, ideal_diff));
            senders.push_back({p, ideal_diff});
        }
        else if (ideal_diff < 0)
        {
            TRACE("recver " VARS(p, -ideal_diff));
            receivers.push_back({p, -ideal_diff});
        }
    }

    std::sort(senders.begin(), senders.end());
    std::sort(receivers.begin(), receivers.end());

    // allocate temporary buffer for sending/receiving a bunch of ineqs
    const size_t max_eqs = senders.empty() ? 0 : senders.back().num_eqs;
    const std::unique_ptr<T[]> tmp(new T[max_eqs * new_eq_size]);

    // execute the stack of operations
    while (!senders.empty() && !receivers.empty())
    {
        sendrecv& s = senders.back();
        sendrecv& r = receivers.back();
        TRACE(VARS(s.rank, s.num_eqs, r.rank, r.num_eqs));

        int num_eqs = std::min(s.num_eqs, r.num_eqs);

        if (s.rank == rank)
        {
            // pack and send
            T * dst = &tmp[0];
            size_t avail_eqs = t.size();
            for (size_t i = avail_eqs - 1; i >= avail_eqs - num_eqs; --i)
            {
                dst = std::copy(std::begin(t[i]), std::end(t[i]), dst);
            }
            MPI_Send(&tmp[0], num_eqs * new_eq_size, MPIType<T>(), r.rank, 0, MPI_COMM_WORLD);
            t.resize(avail_eqs - num_eqs);
        }
        else if (r.rank == rank)
        {
            // receive and unpack
            MPI_Recv(&tmp[0], num_eqs * new_eq_size, MPIType<T>(), s.rank, 0, MPI_COMM_WORLD, NULL);
            for (size_t i = 0; i < num_eqs; ++i)
            {
                t.emplace_back(&tmp[i * new_eq_size], new_eq_size);
            }
        }

        // adjust the counts
        global_counts[3 * s.rank + offset] -= num_eqs;
        global_counts[3 * r.rank + offset] += num_eqs;

        s.num_eqs -= num_eqs;
        r.num_eqs -= num_eqs;

// one-by-one inequality sending -- too slow
//        while (s.num_eqs && r.num_eqs)
//        {
//            if (s.rank == rank)
//            {
//                TRACE("wants to send to " << r.rank);
//                comm::world(r.rank) << t.back();
//                DEBUG("sent " << t.back() << " to " << r.rank);
//                t.pop_back();
//            }
//            else if (r.rank == rank)
//            {
//                t.emplace_back(new_eq_size);
//                TRACE("wants to recv from " << s.rank);
//                comm::world(s.rank) >> t.back();
//                DEBUG("recv " << t.back() << " from " << s.rank);
//            }
//
//            --global_counts[3 * s.rank + offset];
//            ++global_counts[3 * r.rank + offset];
//
//            --s.num_eqs;
//            --r.num_eqs;
//        }

        // pop the satisfied sender/receiver
        if (r.num_eqs == 0)
        {
            receivers.pop_back();
        }

        if (s.num_eqs == 0)
        {
            senders.pop_back();
        }
    }

    // clean up for subsequent use
    senders.clear();
    receivers.clear();
}

template<typename T>
inline void MPIInequalitySolver<T>::CheckArbitrarySolution(bool arbitrary_solution) const
{
    // arbitrary only if in all procs
    int val = arbitrary_solution;
    MPI_Allreduce(MPI_IN_PLACE, &val, 1, MPI_INT, MPI_LAND, MPI_COMM_WORLD);
    if (val)
    {
        throw ArbitrarySolutionException("");
    }
}

template<typename T>
inline void MPIInequalitySolver<T>::CheckNoSolution(bool no_solution) const
{
    // no solution even if only one proc thinks that
    int val = no_solution;
    MPI_Allreduce(MPI_IN_PLACE, &val, 1, MPI_INT, MPI_LOR, MPI_COMM_WORLD);
    if (val)
    {
        if (rank == 0)
        {
            std::cout << "No solution found" << std::endl;
        }
        throw NoSolutionException("");
    }
}

template<typename T>
inline void MPIInequalitySolver<T>::BroadcastCategory(Equations& t, InequalityType type, size_t new_eq_size)
{
    size_t offset = type;

    // determine the max count of ineqs that'll be sent
    size_t max_ineqs = 0;
    for (int p = 0; p < procs; ++p)
    {
        if (global_counts[3 * p + offset] > max_ineqs)
        {
            max_ineqs = global_counts[3 * p + offset];
        }
    }
//======== using Bcasts ========
    // alloc temp buffer
    const std::unique_ptr<T[]> tmp(new T[max_ineqs * new_eq_size]);

    // bcast from every process
    for (int p = 0; p < procs; ++p)
    {
        const size_t num_eqs = global_counts[3 * p + offset];
        if (num_eqs == 0)
        {
            continue;
        }

        // pack if root
        TRACE(VARS(num_eqs, tmp.size()));
        if (p == rank)
        {
            T * dst = &tmp[0];
            for (size_t i = 0; i < num_eqs; ++i)
            {
                dst = std::copy(std::begin(t[i]), std::end(t[i]), dst);
            }
        }

        // everyone performs the bcast
        MPI_Bcast(&tmp[0], int(num_eqs * new_eq_size), MPIType<T>(), p, MPI_COMM_WORLD);
        DEBUG("bcast " << num_eqs << " ineqs");

        // others only unpack
        if (p != rank)
        {
            for (size_t i = 0; i < num_eqs; ++i)
            {
                t.emplace_back(&tmp[i * new_eq_size], new_eq_size);
            }
        }
    }

//======== using Allgather ========
//    const std::unique_ptr<T[]> tmp(new T[procs * max_ineqs * new_eq_size]);
//
//    T * dst = &tmp[rank * max_ineqs * new_eq_size];
//    for (const auto& e : t)
//    {
//        dst = std::copy(std::begin(e), std::end(e), dst);
//    }
//
//    MPI_Allgather(MPI_IN_PLACE, 0, MPI_DATATYPE_NULL, &tmp[0], max_ineqs * new_eq_size, MPIType<T>(), MPI_COMM_WORLD);
//
//    for (int p = 0; p < procs; ++p)
//    {
//        if (p == rank)
//        {
//            continue;
//        }
//        T * src = &tmp[p * max_ineqs * new_eq_size];
//        const size_t num_eqs = global_counts[3 * p + offset];
//        for (size_t i = 0; i < num_eqs; ++i)
//        {
//            t.emplace_back(src + i * new_eq_size, new_eq_size);
//        }
//    }

//======== using matching send/recv ========
//    size_t limit = (type == Plus ? global_count_plus : global_count_minus)- t.size();
//
//    size_t sent = 0;
//    for (int p = 0; p < procs; ++p)
//    {
//        if (p == rank)
//        {
//            continue;
//        }
//
//        for (const auto& e : t)
//        {
//            comm::world(p).isend(e);
//            DEBUG("sent " << e << " to " << p);
//            ++sent;
//        }
//    }
//
//    size_t total_eq = global_count_plus + global_count_minus + global_count_zero;
//    INFO(VARS(global_count_plus, global_count_minus, global_count_zero, total_eq));
//
//    size_t recvd = 0;
//    for (size_t i = 0; i < limit; ++i)
//    {
//        t.emplace_back(new_eq_size);
//        if (LOG_LEVEL >= DEBUG_LEVEL)
//        {
//            status s = comm::world(any) >> t.back();
//            DEBUG("recv " << t.back() << " from " << s.source().rank());
//        }
//        else
//        {
//            comm::world(any) >> t.back();
//        }
//        ++recvd;
//
//    }
}

template<typename T>
inline void MPIInequalitySolver<T>::BroadcastCategories(size_t new_eq_size)
{
    TRACE("record bounds " << new_eq_size << VARS(t_plus.size(), t_minus.size(), t_zero.size()));

    // broadcast the smaller set of ineqs
    if (global_count_plus < global_count_minus)
    {
        BroadcastCategory(t_plus, Plus, new_eq_size);
    }
    else
    {
        BroadcastCategory(t_minus, Minus, new_eq_size);
    }
}

template<typename T>
inline void MPIInequalitySolver<T>::GatherBounds(T& lb, T& ub, bool& lb_inf, bool& ub_inf) const
{
    // local class to represent bounds in all procs
    struct bounds
    {
        bounds(T * p)
        : lb(p[0]),
          ub(p[1]),
          lb_inf(int(p[2])),
          ub_inf(int(p[3])) {}
        T& lb;
        T& ub;
        bool lb_inf;
        bool ub_inf;
    };

    T to_send[] = {lb, ub, T(int(lb_inf)), T(int(ub_inf))};
    // static buffer to hold all the bounds
    static const std::unique_ptr<T[]> to_recv(new T[4 * procs]);
    // everyone gets all the bounds
    MPI_Allgather(to_send, 4, MPIType<T>(), &to_recv[0], 4, MPIType<T>(), MPI_COMM_WORLD);
    TRACE("gathered bounds");

    // check if infinite
    for (size_t i = 0; i < 4 * procs; i += 4)
    {
       bounds b(&to_recv[i]);
       lb_inf &= b.lb_inf;
       ub_inf &= b.ub_inf;
    }

    if (lb_inf && ub_inf)
    {
        return;
    }

    // if at least one is finite
    bool lb_set = false, ub_set = false;
    for (size_t i = 0; i < 4 * procs; i += 4)
    {
        bounds b(&to_recv[i]);
        // find the maximal finite lower bound
        if (!lb_set && !b.lb_inf)
        {
            lb = b.lb;
            lb_set = true;
        }
        else if (lb_set && !b.lb_inf && b.lb > lb)
        {
            lb = b.lb;
        }

        // and find the minimal finite upper bound
        if (!ub_set && !b.ub_inf)
        {
            ub = b.ub;
            ub_set = true;
        }
        else if (ub_set && !b.ub_inf && b.ub < ub)
        {
            ub = b.ub;
        }
    }
}

template<typename T>
inline void MPIInequalitySolver<T>::OutOfMemory()
{
    std::cerr << "rank " << rank << ": " << "Insufficient memory, terminating..." << std::endl;
    // just have to bail out
    MPI_Abort(MPI_COMM_WORLD, -1);
}

template<typename T>
inline void MPIInequalitySolver<T>::UnknownException(const std::exception& e)
{
    std::cerr << "rank " << rank << ": " << e.what() << std::endl;
    // unexpected problem -> bail out
    MPI_Abort(MPI_COMM_WORLD, -2);
}

#endif /* MPIINEQUALITYSOLVER_H_ */
