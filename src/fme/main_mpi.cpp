#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>

#include <ostream>
#include <fstream>
#include <sstream>
#include <cassert>
#include <iomanip>

#include <gperftools/malloc_extension.h>

#include <mpi.h>
#include "mpp.h"

#undef PUT_RANK
#define PUT_RANK 1
#undef LOG_LEVEL
#define LOG_LEVEL ERROR_LEVEL
#include "Logging.h"
#include "MPIInequalitySolver.h"

using namespace std;

int main(int argc, char ** argv)
{
    ios::sync_with_stdio(false);

    if (argc < 3)
    {
        cerr << "Usage: mpirun -np # ./fme_mpi input_file i|l|f|d [all]" << endl;
        return 0;
    }

    mpi::init(argc, argv);

    // set the memory allocation limit
    rlimit rl;
    getrlimit(RLIMIT_AS, &rl);

    rl.rlim_cur = long(sysconf(_SC_PHYS_PAGES) * sysconf(_SC_PAGESIZE) * 0.95);
    rl.rlim_cur /= std::min(sysconf(_SC_NPROCESSORS_ONLN), long(comm::world.size()));

    setrlimit(RLIMIT_AS, &rl);

    bool all = false;
    if (argc == 4)
        all = true;

    // open the input file
    ifstream in(argv[1]);
    if (!in)
    {
        if (comm::world.rank() == 0)
        {
            cerr << "Cannot open input file " << argv[1] << endl;
        }
        mpi::finalize();
        return 0;
    }

    size_t memory_usage = 0;
    bool solution_exists = false;
    double t1, t2;

    // sync and take timestamp
    MPI_Barrier(MPI_COMM_WORLD);
    t1 = MPI_Wtime();

#define SOLVE_TYPE(DataType) do \
    { \
        MPIInequalitySolver<DataType> is; \
        is.Read(in); \
        \
        if (all) \
        { \
            MPIInequalitySolver<DataType>::Result lower_bounds, upper_bounds; \
            solution_exists = is.Solve(lower_bounds, upper_bounds); \
            MPI_Barrier(MPI_COMM_WORLD); \
            t2 = MPI_Wtime(); \
            MallocExtension::instance()->GetNumericProperty("generic.current_allocated_bytes", &memory_usage); \
            if (solution_exists && comm::world.rank() == 0) \
            { \
                is.PrintSolution(cout, lower_bounds, upper_bounds); \
            } \
        } \
        else \
        { \
            MPIInequalitySolver<DataType>::Result result; \
            solution_exists = is.Solve(result); \
            MPI_Barrier(MPI_COMM_WORLD); \
            t2 = MPI_Wtime(); \
            MallocExtension::instance()->GetNumericProperty("generic.current_allocated_bytes", &memory_usage); \
            if (solution_exists && comm::world.rank() == 0) \
            { \
                is.PrintSolution(cout, result); \
            } \
        } \
    } while (0)

    // instantiate with the give data type
    switch (argv[2][0])
    {
    case 'i':
        SOLVE_TYPE(int);
        break;
    case 'l':
        SOLVE_TYPE(long);
        break;
    case 'f':
        SOLVE_TYPE(float);
        break;
    case 'd':
        SOLVE_TYPE(double);
        break;
    default:
        if (comm::world.rank() == 0)
        {
            cerr << "Invalid data type, use only i, l, f, d" << endl;
        }
        mpi::finalize();
        return 0;
    }

    // sum up the memory usage from all procs
    size_t total_memory = 0;
    MPI_Reduce(&memory_usage, &total_memory, 1, MPIType<size_t>(), MPI_SUM, 0, MPI_COMM_WORLD);
    if (comm::world.rank() == 0)
    {
        cout << "time: " << t2 - t1 << "s\n"
             << "memory usage: " << fixed << setprecision(3)
             << double(total_memory) / 1073741824 << "GB" << endl;
    }

    mpi::finalize();
    return 0;
}

