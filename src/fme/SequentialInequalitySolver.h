#ifndef SEQUENTIALINEQUALITYSOLVER_H_
#define SEQUENTIALINEQUALITYSOLVER_H_

#include <vector>
#include <iostream>
#include <algorithm>
#include <valarray>
#include <numeric>
#include <sstream>

#include "Numerics.h"
#include "Exceptions.h"

/**
 * Forward declaration of the unit test of this class
 */
template <typename U>
class SequentialTest;

/**
 * The SequentialInequalitySolver class is a class representing a sequential solver
 * of systems of linear inequalities.
 */
template <typename T>
class SequentialInequalitySolver
{
public:
    /**
     * The Result type for representing the upper and lower bounds of the variables.
     */
    typedef std::vector<T> Result;

    /**
     * Constructor, assigns necessary variables.
     */
    SequentialInequalitySolver();

    /**
     * Virtual destructor needed because of presence of virtual methods
     */
    virtual ~SequentialInequalitySolver() {}

    /**
     * A method encapsulating reading of systems of linear inequalities.
     * @param is an already opened input stream to read from
     */
    virtual void Read(std::istream& is);

    /**
     * The main method for solving systems of linear inequalities. Finds one solution satisfying the system.
     * @param result a vector of values satisfying the given system
     * @return whether there exists a solution to the given system
     */
    bool Solve(Result& result);

    /**
     * The main method for solving systems of linear inequalities. Tries to find integral ranges of all
     * unknowns.
     * @param lower_bounds a vector of lower bounds for the unknowns
     * @param upper_bounds a vector of upper bounds for the unknowns
     * @return whether there exists a solution to the given system
     */
    bool Solve(Result& lower_bounds, Result& upper_bounds);

    //@{
    /**
     * Routines for convenient printing of results.
     * @param os the stream to output to
     * @param result the result to print
     * @param lower_bounds the lower bounds to print
     * @param upper_bounds the upper bounds to print
     */
    static void PrintSolution(std::ostream& os, const Result& result);
    static void PrintSolution(std::ostream& os, const Result& lower_bounds, const Result& upper_bounds);
    //@}

protected:
    typedef std::size_t size_t;                         /**< typedef for array indices etc. */
    typedef std::valarray<T> Equation;                  /**< typedef for an equation representation */
    typedef std::vector<Equation> Equations;            /**< typedef for the representation of a system */
    typedef std::vector<Equations> Bounds;              /**< typedef for storing bounds for later use */
    typedef std::valarray<int_t> IntEquation;           /**< typedef for equations of int_t */

    /**
     * A method that does the actual reading of a textual representation of a system of linear inequalities.
     */
    void DoRead(std::istream& is);

    /**
     * The private solving method, called accordingly by the public ones.
     * @param lower_bounds a vector of lower bounds for the unknowns
     * @param upper_bounds a vector of upper bounds for the unknowns
     * @param get_bounds whether to try to find the integral ranges of all unknowns
     * @return whether there exists a solution to the given system
     */
    bool Solve(Result& lower_bounds, Result& upper_bounds, bool get_bounds);

    /**
     * An empty hook method to be overridden by derived classes.
     */
    virtual void RedistributeInequalities(size_t) {}

    /**
     * Normalises all inequalities with non-zero last coefficient based on it.
     * @param col the variable index to normalise to
     */
    void Normalise(size_t col);

    /**
     * An empty hook method to be overridden by derived classes.
     */
    virtual void BroadcastCategories(size_t) {}

    /**
     * Saves the upper and lower bounds for variable col for later use in back substitution.
     * @param new_eq_size the size of incoming inequalities
     */
    void RecordBounds(size_t new_eq_size);

    /**
     * Reacts to arbitrary solution accordingly by throwing or not throwing an exception.
     * @param arbitrary_solution whether the solution can be arbitrary
     */
    virtual void CheckArbitrarySolution(bool arbitrary_solution) const;

    /**
     * Reacts to no solution accordingly by throwing or not throwing an exception.
     * @param no_solution indicates non-existence of a solution
     */
    virtual void CheckNoSolution(bool no_solution) const;

    /**
     * Eliminates the variable with index col by producing new inequalities of col-1 variables.
     * @param col the variable index to eliminate
     */
    void Eliminate(size_t col);

    /**
     * Performs back substitution after eliminating all but one variables.
     * @param result1 a vector of either one solution or the lower bounds
     * @param result2 a vector of upper bounds, filled in only when get_bounds is true
     * @param get_bounds whether to find the ranges of all unknowns, not possible with infinite bounds
     */
    void BackSubstitute(Result& result1, Result& result2, bool get_bounds, bool arbitrary = false);

    /**
     * Finds the actual upper and lower bounds for x_n, assuming values of x_1..x_n-1 in param x.
     * @param x values of x_1..x_n-1
     * @param i how many coefficients of the equation to take into account
     * @param lb_inf[out] whether the resulting lower bound is -infinity
     * @param ub_inf[out] whether the resulting upper bound is +infinity
     * @param lb[out] the actual value of the lower bound if not -infinity
     * @param ub[out] the actual value of the upper bound if not +infinity
     */
    void GetBounds(const Equation& x, size_t i, bool& lb_inf, bool& ub_inf, T& lb, T& ub) const;

    /**
     * An empty hook method to be overridden by derived classes.
     */
    virtual void GatherBounds(T&, T&, bool&, bool&) const {}

    /**
     * Recursive method for finding integral ranges of all unknowns. Iterative approach
     * in BackSubstitute used instead.
     * @param x current values of unknowns
     * @param col index of the unknown to be brute-force tried
     * @param x_l lower bounds for current variable, given by values of other vars
     * @param x_u upper bounds for current variable, given by values of other vars
     */
    void RecurseThroughBounds(Equation& x, size_t col, IntEquation& x_l, IntEquation& x_u);

    /**
     * Places inequality to the corresponding set (+, - or 0) based on the last index.
     * @param e inequality to place
     * @param i index to base on
     */
    void PlaceInequality(Equation& e, size_t i);

    /**
     * A debug print method for easy checking the current values of the whole system.
     */
    std::string DebugPrint() const;

    /**
     * A method called when std::bad_alloc is caught, can be overloaded.
     */
    virtual void OutOfMemory();

    /**
     * A method call when an unexpected exception is caught, can be overloaded.
     * @param e the exception
     */
    virtual void UnknownException(const std::exception& e);

    /**
     * Evaluates an equation e using values x by subtracting the sum of products of coefficients
     * with variables from the right hand side of the normalised equation.
     * @param e the equation to evaluate
     * @param x the values of known variables
     * @param i how many coefficients of the equation to take into account
     * @return the evaluation of e with values x subtracted from e[0]
     */
    static T Eval(const Equation& e, const Equation& x, size_t i);

    Equations t_plus;                   /**< inequalities with positive last coefficient */
    Equations t_minus;                  /**< inequalities with negative last coefficient */
    Equations t_zero;                   /**< inequalities with zero last coefficient */
    Bounds lower_bounds, upper_bounds;  /**< bounds for the back substitution, recorded during the algorithm */
    size_t unknowns;                    /**< number of unknowns in the system */
    Equations t_orig;                   /**< the original input, saved for substitution check */

    /**
     * Make the unit test of this class a friend to be able to see private members
     */
    template <typename U>
    friend class SequentialTest;
};

template<typename T>
SequentialInequalitySolver<T>::SequentialInequalitySolver()
: unknowns(0)
{

}

template<typename T>
inline void SequentialInequalitySolver<T>::PrintSolution(std::ostream& os, const Result& result)
{
    os << '(';
    for (auto i = result.begin(); i != result.end(); ++i)
    {
        os << *i;
        if (i != result.end() - 1)
            os << ", ";
    }
    os << ")\n";
}

template<typename T>
inline void SequentialInequalitySolver<T>::PrintSolution(std::ostream& os, const Result& lower_bounds,
                                                         const Result& upper_bounds)
{
    auto il = lower_bounds.begin();
    auto iu = upper_bounds.begin();
    do
    {
        os << '[' << *il++ << ", " << *iu++ << "]\n";
    } while (il != lower_bounds.end() && iu != upper_bounds.end());
}

template<typename T>
inline T SequentialInequalitySolver<T>::Eval(const Equation& e, const Equation& x, size_t i)
{
    // substitute all known xs and subtract from the RHS
    return e[0] - std::inner_product(std::begin(e) + 1, std::begin(e) + i, std::begin(x), T(0));
}

template<typename T>
inline void SequentialInequalitySolver<T>::PlaceInequality(Equation& e, size_t i)
{
    // look at e[i] and place into corresponding category
    if (IsZero(e[i]))
    {
        move_back(t_zero, e);
    }
    else if (e[i] > 0)
    {
        move_back(t_plus, e);
    }
    else if (e[i] < 0)
    {
        move_back(t_minus, e);
    }
    else
    {
        // this should not happen
        std::ostringstream os;
        os << e;
        throw std::domain_error("Cannot classify inequality " + os.str());
    }
}

template<typename T>
inline void SequentialInequalitySolver<T>::DoRead(std::istream& is)
{
    size_t n, m;
    is >> n;
    is >> m;
    unknowns = m;

    // clear category containers
    t_plus.clear();
    t_minus.clear();
    t_zero.clear();

    t_orig.clear();
    for (size_t i = 0; i < n; ++i)
    {
        Equation e(m + 1);

        // read the actual values
        for (size_t j = 1; j < m + 1; ++j)
        {
            is >> e[j];
        }
        bool greater = false;

        char c;
        is.get(c);
        is.get(c);
        if (c == '>')
        {
            greater = true;
        }

        is >> e[0];
        if (greater)    // flip the sign if '>' is encountered
        {
            e = -e;
        }

        move_back(t_orig, e);

    }
}

template<typename T>
inline void SequentialInequalitySolver<T>::Read(std::istream& is)
{
    DoRead(is);

    // clear bounds and reserve space for new
    lower_bounds.clear();
    upper_bounds.clear();

    lower_bounds.reserve(unknowns);
    upper_bounds.reserve(unknowns);

    // place the read ineqs
    Equations t(t_orig);
    for (auto& e : t)
    {
        PlaceInequality(e, unknowns);
    }
}

template<typename T>
inline bool SequentialInequalitySolver<T>::Solve(Result& result)
{
    Result upper_bounds;
    return Solve(result, upper_bounds, false);
}

template<typename T>
inline bool SequentialInequalitySolver<T>::Solve(Result& lower_bounds, Result& upper_bounds)
{
    return Solve(lower_bounds, upper_bounds, true);
}

template<typename T>
inline void SequentialInequalitySolver<T>::OutOfMemory()
{
    std::cerr << "Insufficient memory, terminating..." << std::endl;
}

template<typename T>
inline void SequentialInequalitySolver<T>::UnknownException(const std::exception& e)
{
    std::cerr << e.what() << std::endl;
}

template<typename T>
inline bool SequentialInequalitySolver<T>::Solve(Result& lower_bounds, Result& upper_bounds, bool get_bounds)
{
    // the main algorithm loop
    try
    {
        for (size_t i = unknowns; i > 1; --i)
        {
            RedistributeInequalities(i + 1);
            Normalise(i);
            RecordBounds(i + 1);
            Eliminate(i);
        }
        Normalise(1);
        BackSubstitute(lower_bounds, upper_bounds, get_bounds);
    }
    catch (const ArbitrarySolutionException& e)
    {
        INFO("Arbitrary solution");
        BackSubstitute(lower_bounds, upper_bounds, false, true);
    }
    catch (const NoSolutionException& e)
    {
        return false;
    }
    catch (const std::bad_alloc& e)
    {
        OutOfMemory();
        return false;
    }
    catch (const std::exception& e)
    {
        ERROR("unexpected exception");
        UnknownException(e);
        return false;
    }
    return true;
}

template<typename T>
inline void SequentialInequalitySolver<T>::Normalise(size_t col)
{
    for (auto& e : t_plus)
    {
        e /= e[col];  // divide the whole equation by the coefficient at col
    }
    for (auto& e : t_minus)
    {
        e /= e[col];  // divide the whole equation by the coefficient at col
    }
}

template<>
inline void SequentialInequalitySolver<float>::Normalise(size_t col)
{
    for (auto& e : t_plus)
    {
        float factor = 1.0f / e[col];
        e *= factor;  // divide the whole equation by the coefficient at col
    }
    for (auto& e : t_minus)
    {
        float factor = 1.0f / e[col];
        e *= factor;  // divide the whole equation by the coefficient at col
    }
}

template<>
inline void SequentialInequalitySolver<double>::Normalise(size_t col)
{
    for (auto& e : t_plus)
    {
        double factor = 1.0 / e[col];
        e *= factor;  // divide the whole equation by the coefficient at col
    }
    for (auto& e : t_minus)
    {
        double factor = 1.0 / e[col];
        e *= factor;  // divide the whole equation by the coefficient at col
    }
}

template<typename T>
inline void SequentialInequalitySolver<T>::RecordBounds(size_t new_eq_size)
{
    BroadcastCategories(new_eq_size);

    if (t_plus.empty() || t_minus.empty()) // upper or lower bound is +inf
    {
        DEBUG("infinite bound");
    }

    // record the actual bounds
    move_back(lower_bounds, t_minus);
    t_minus.clear();
    move_back(upper_bounds, t_plus);
    t_plus.clear();
}

template<typename T>
inline void SequentialInequalitySolver<T>::CheckArbitrarySolution(bool arbitrary_solution) const
{
    if (arbitrary_solution)
    {
        throw ArbitrarySolutionException("");
    }
}

template<typename T>
inline void SequentialInequalitySolver<T>::Eliminate(size_t col)
{
    // check arbitrary solution
    bool arbitrary_solution = false;
    if (t_zero.empty() && (upper_bounds.back().empty() || lower_bounds.back().empty()))
    {
        arbitrary_solution = true;
    }

    CheckArbitrarySolution(arbitrary_solution);

    // truncate the ineqs with zero as the last coeff
    Equations tmp = std::move(t_zero);
    t_zero.clear();

    for (const auto& z : tmp)
    {
        Equation e(&z[0], col);
        PlaceInequality(e, col - 1);
    }

    // allocate space for new ineqs if it is not too much
    size_t new_size = (upper_bounds.back().size() * lower_bounds.back().size()) / 2;
    if (new_size < 1000000)
    {
        t_plus.reserve(new_size);
        t_minus.reserve(new_size);
    }

    // subtract the lower bound ineqs from upper bound ineqs
    // don't look at the last coeff, it's 1
    std::slice s(0, col, 1);
    for (const auto& ub : upper_bounds.back())
    {
        for (const auto& lb : lower_bounds.back())
        {
            Equation e(ub[s] - lb[s]);
            PlaceInequality(e, col - 1);
        }
    }

    TRACE("eliminate " VARS(t_plus.size(), t_minus.size(), t_zero.size()));
}

template<typename T>
inline void SequentialInequalitySolver<T>::CheckNoSolution(bool no_solution) const
{
    if (no_solution)
    {
        std::cout << "No solution found" << std::endl;
        throw NoSolutionException("");
    }
}

template<typename T>
inline void SequentialInequalitySolver<T>::BackSubstitute(Result& result1, Result& result2, bool get_bounds,
                                                          bool arbitrary)
{
    // compare functor for std::min/max_element, looks at the first coeff
    struct eq_cmp
    {
        bool operator()(const Equation& a, const Equation& b) const
        {
            return a[0] < b[0];
        }
    };
    T ub, lb;
    bool ub_inf = false, lb_inf = false;

    // find the bounds for the last unknown if not arbitrary
    if (!arbitrary)
    {
        if (t_plus.empty())  // upper bound +inf
        {
            ub_inf = true;
        }
        else
        {
            ub = (*std::min_element(t_plus.begin(), t_plus.end(), eq_cmp()))[0];
        }

        if (t_minus.empty())  // lower bound -inf
        {
            lb_inf = true;
        }
        else
        {
            lb = (*std::max_element(t_minus.begin(), t_minus.end(), eq_cmp()))[0];
        }

        GatherBounds(lb, ub, lb_inf, ub_inf);

        // check if there is a solution
        bool no_solution = false;
        // upper bound cannot be lower than lower bound
        if (!(ub_inf || lb_inf) && ub < lb)
        {
            no_solution = true;
        }

        // 0 < C has to hold
        for (const auto& e : t_zero)
        {
            if (e[0] < T(0))
            {
                no_solution = true;
                break;
            }
        }
        CheckNoSolution(no_solution);

    }


    Equation x(T(0), unknowns);

    TRACE(VARS(lb, ub));

    if (get_bounds)
    {
        // cannot enumerate if infinite
        if (ub_inf || lb_inf)
        {
            throw InenumerableSolutionException("Cannot enumerate all - infinite bounds!");
        }

        result1.resize(unknowns, static_cast<T>(std::numeric_limits<int_t>::max()));
        result2.resize(unknowns, static_cast<T>(std::numeric_limits<int_t>::min()));

        IntEquation x_l(unknowns), x_u(unknowns);
        x_l[0] = Ceiling(lb);
        x_u[0] = Floor(ub);

        //Not used, iterative approach below used instead
        //RecurseThroughBounds(x, 1, x_l, x_u);

        // stack structure to emulate recursion
        struct recurse
        {
            size_t col;
            int_t i;
            bool sw;
        };
        std::vector<recurse> stack;

        stack.push_back({1, x_l[0], false});

        do
        {
            recurse r = stack.back();
            stack.pop_back();

            // enumerate the integer bounds for all combinations
            for (int_t i = r.sw ? x_l[r.col - 1] : r.i; i <= x_u[r.col - 1]; ++i)
            {
                x[r.col - 1] = T(i);

                // found a solution vector, check if the bounds grew
                if (r.col == unknowns)
                {
                    for (size_t j = 0; j < x.size(); ++j)
                    {
                        if (x_l[j] < result1[j])
                        {
                            result1[j] = T(x_l[j]);
                        }
                        if (x_u[j] > result2[j])
                        {
                            result2[j] = T(x_u[j]);
                        }
                    }
                }
                // keep trying
                else
                {
                    GetBounds(x, r.col + 1, lb_inf, ub_inf, lb, ub);

                    GatherBounds(lb, ub, lb_inf, ub_inf);

                    x_l[r.col] = Ceiling(lb);
                    x_u[r.col] = Floor(ub);
                    stack.push_back({r.col, i + 1, false});
                    stack.push_back({r.col + 1, i, true});
                    break;
                }
            }
        } while (!stack.empty());
    }
    else
    {
        // if arbitrary start from last eliminated variable
        size_t i;
        if (arbitrary)
        {
            i = unknowns - lower_bounds.size();
        }
        // else start from the actual last variable
        else
        {
            x[0] = ChooseFromInterval(lb_inf, ub_inf, lb, ub);
            i = 1;
        }

        do
        {
            // get bounds for the current unknown
            GetBounds(x, i + 1, lb_inf, ub_inf, lb, ub);

            GatherBounds(lb, ub, lb_inf, ub_inf);
            TRACE(VARS(lb, ub, lb_inf, ub_inf));

            // choose from between the bounds
            x[i++] = ChooseFromInterval(lb_inf, ub_inf, lb, ub);
        } while (i < unknowns);


        // check if the solution satisfies the original input system
        for (const auto& e : t_orig)
        {
            T r = std::inner_product(std::begin(e) + 1, std::end(e), std::begin(x), T(0));
            if (r > e[0])
            {
                CRITICAL_ERROR("not satisfying the original system");
            }
        }

        result1.assign(std::begin(x), std::end(x));
    }
}

template<typename T>
void SequentialInequalitySolver<T>::RecurseThroughBounds(Equation& x, size_t col, IntEquation& x_l,
                                                         IntEquation& x_u)
{
    for (int_t i = x_l[col - 1]; i <= x_u[col - 1]; ++i)
    {
        x[col - 1] = i;

        if (col == unknowns)
        {
            // do whatever is needed with the current x
        }
        else
        {
            T lb, ub;
            bool lb_inf, ub_inf;
            GetBounds(x, col + 1, lb_inf, ub_inf, lb, ub);
            x_l[col] = Ceiling(lb);
            x_u[col] = Floor(ub);
            RecurseThroughBounds(x, col + 1, x_l, x_u);
        }
    }
}

template<typename T>
inline void SequentialInequalitySolver<T>::GetBounds(const Equation& x, size_t i, bool& lb_inf, bool& ub_inf,
                                                     T& lb, T& ub) const
{
    ub_inf = lb_inf = false;

    // find the upper bound by evaluating all the bound ineqs
    const Equations& ubs = upper_bounds[unknowns - i];
    if (ubs.empty())
    {
        ub_inf = true;
    }
    else
    {   // find the minimum of upper bounds
        typename Equations::const_iterator it = ubs.begin();
        ub = Eval(*it, x, i);
        while (++it != ubs.end())
        {
            T curr = Eval(*it, x, i);
            if (curr < ub)
            {
                ub = curr;
            }
        }
    }

    // find the lower bound by evaluating all the bound ineqs
    const Equations& lbs = lower_bounds[unknowns - i];
    if (lbs.empty())
    {
        lb_inf = true;
    }
    else
    {   // find the maximum of lower bounds
        typename Equations::const_iterator it = lbs.begin();
        lb = Eval(*it, x, i);
        while (++it != lbs.end())
        {
            T curr = Eval(*it, x, i);
            if (curr > lb)
            {
                lb = curr;
            }
        }
    }
}

template<typename T>
inline std::string SequentialInequalitySolver<T>::DebugPrint() const
{
    std::ostringstream os;
    os << "\n----------------\n";
    for (const auto& e : t_plus)
    {
        os << e << '\n';
    }
    for (const auto& e : t_minus)
    {
        os << e << '\n';
    }
    for (const auto& e : t_zero)
    {
        os << e << '\n';
    }
    os << "----------------\n";
    return os.str();
}

#endif /* SEQUENTIALINEQUALITYSOLVER_H_ */
