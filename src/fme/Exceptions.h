#ifndef EXCEPTIONS_H_
#define EXCEPTIONS_H_

#include <stdexcept>

class NoSolutionException : public std::runtime_error
{
public:
    NoSolutionException(const std::string& msg)
    : std::runtime_error("No solution found - " + msg) { }
};

class ArbitrarySolutionException : public std::runtime_error
{
public:
    ArbitrarySolutionException(const std::string& msg) : std::runtime_error(msg) { }
};

class InenumerableSolutionException : public std::runtime_error
{
public:
    InenumerableSolutionException(const std::string& msg) : std::runtime_error(msg) { }
};

#endif /* EXCEPTIONS_H_ */
