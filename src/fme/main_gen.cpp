#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

long GetRandom(long min, long max)
{
    return min + rand() % (max - min);
}

int main(int argc, char ** argv)
{
    if (argc < 4)
    {
        cerr << "Usage: gen num_eqs num_vars k [probability of zero]" << endl;
        return 1;
    }
    ios::sync_with_stdio(false);
    size_t eqs, vars;
    float zeros = 0.3f;
    eqs = strtoul(argv[1], NULL, 10);
    vars = strtoul(argv[2], NULL, 10);
    long min, max;
    max = strtol(argv[3], NULL, 10);
    min = -max;


    if (argc > 4)
        zeros = strtof(argv[4], NULL);

    srand(unsigned(time(NULL)));

    cout << eqs << ' ' << vars << '\n';
    for (size_t i = 0; i < eqs; ++i)
    {
        for (size_t j = 0; j < vars; ++j)
        {
            if ((float) rand() / RAND_MAX > zeros)
                cout << GetRandom(min, max);
            else
                cout << '0';
            cout << ' ';
        }
        cout << "< " << GetRandom(500000 * max, 1000000 * max) + long(1000000*((float) rand() / RAND_MAX)) << '\n';
    }

    return 0;
}
