#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>

#include <iostream>
#include <fstream>
#include <chrono>
#include <iomanip>

#include <gperftools/malloc_extension.h>

#undef LOG_LEVEL
#define LOG_LEVEL ERROR_LEVEL
#include "Logging.h"
#include "SequentialInequalitySolver.h"

using namespace std;

int main(int argc, char ** argv)
{
    ios::sync_with_stdio(false);

    if (argc < 3)
    {
        cerr << "Usage: fme_seq input_file i|l|f|d|Z|R|Q [all]" << endl;
        return 1;
    }

    bool all = false;
    if (argc == 4)
        all = true;

    // try opening the input
    ifstream in(argv[1]);
    if (!in)
    {
        cerr << "Cannot open input file " << argv[1] << endl;
        return 1;
    }

    // set the memory allocation limit
    rlimit rl;
    getrlimit(RLIMIT_AS, &rl);

    rl.rlim_cur = long(sysconf(_SC_PHYS_PAGES) * sysconf(_SC_PAGESIZE) * 0.95);

    setrlimit(RLIMIT_AS, &rl);

    size_t memory_usage = 0;
    bool solution_exists = false;
    chrono::high_resolution_clock::time_point start, end;
    // measure time
    start = chrono::high_resolution_clock::now();

#define SOLVE_TYPE(DataType) do \
    { \
        SequentialInequalitySolver<DataType> is; \
        is.Read(in); \
        \
        if (all) \
        { \
            SequentialInequalitySolver<DataType>::Result lower_bounds, upper_bounds; \
            solution_exists = is.Solve(lower_bounds, upper_bounds); \
            end = chrono::high_resolution_clock::now(); \
            MallocExtension::instance()->GetNumericProperty("generic.current_allocated_bytes", &memory_usage); \
            if (solution_exists) \
            { \
                is.PrintSolution(cout, lower_bounds, upper_bounds); \
            } \
        } \
        else \
        { \
            SequentialInequalitySolver<DataType>::Result result; \
            solution_exists = is.Solve(result); \
            end = chrono::high_resolution_clock::now(); \
            MallocExtension::instance()->GetNumericProperty("generic.current_allocated_bytes", &memory_usage); \
            if (solution_exists) \
            { \
                is.PrintSolution(cout, result); \
            } \
        } \
    } while (0)

    // instantiate with the give data type
    switch (argv[2][0])
    {
    case 'i':
        SOLVE_TYPE(int);
        break;
    case 'l':
        SOLVE_TYPE(long);
        break;
    case 'f':
        SOLVE_TYPE(float);
        break;
    case 'd':
        SOLVE_TYPE(double);
        break;
    case 'Q':
        SOLVE_TYPE(mpq_class);
        break;
    case 'Z':
        SOLVE_TYPE(mpz_class);
        break;
    case 'R':
        SOLVE_TYPE(mpf_class);
        break;
    default:
        cerr << "Invalid data type, use only i, l, f, d, Z, R, Q" << endl;
        return 1;
        break;
    }

    auto time = chrono::duration_cast<chrono::microseconds>(end - start).count();
    cout << "time: " << time / 1000000.0 << "s\n"
         << "memory usage: " << fixed << setprecision(3)
         << double(memory_usage) / 1073741824 << "GB" << endl;

    return !solution_exists;
}

