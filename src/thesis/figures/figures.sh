#!/bin/sh
for f in *.tex 
do
    pdflatex -shell-escape -synctex=1 -interaction=nonstopmode $f >/dev/null
done
