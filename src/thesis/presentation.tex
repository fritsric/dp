\documentclass{beamer}
\usefonttheme[onlymath]{serif}
\usepackage[utf8]{inputenc}
\usepackage{mathpazo}
\renewcommand*\sfdefault{uop}
\usepackage[scaled=0.9]{ulgothic} % tt font
\usepackage[T1]{fontenc}
\usecolortheme{whale}

\title{Parallel Solver of Large Systems of Linear Inequalities}
\subtitle{Master's thesis}
\author{Richard Fritsch}
\institute
{
Czech Technical University in Prague\\
Faculty of Information Technology\\
\includegraphics[scale=0.5]{cvut-logo-bw}
}
\date{June 2014}
%family=\sffamily,series=\bfseries,size=\Huge
\setbeamerfont{subtitle}{family=\sffamily,series=\mdseries}
\setbeamerfont{title}{family=\sffamily,series=\bfseries}
\setbeamerfont{author}{family=\sffamily,series=\itshape}
\setbeamerfont{date}{family=\sffamily,size=\footnotesize}
\setbeamerfont{institute}{family=\sffamily,size=\footnotesize}

\setbeamerfont{frametitle}{family=\sffamily,series=\bfseries}


\begin{document}

\frame{\titlepage}

\begin{frame}
\frametitle{Thesis Goals}
\large\begin{itemize}
\item Analyse, design and implement a parallel solver
for large systems of linear inequalities
\item Utilise Fourier--Motzkin elimination and MPI library
\item Ensure speed and memory efficiency
\item Evaluate results
\item Compare with theoretical expectations
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Problem Description}
\vspace{-.4cm}
\begin{itemize}
\item \(n\)~linear inequalities in \(m\)~variables: \(\mathbf{Ax} \leq \mathbf b \text{ with } \mathbf A \in \mathbb R^{n,m} \text{, } \mathbf b \in \mathbb R^n\) \vspace{-.1cm}
\begin{center}
\includegraphics{figures/example}
\end{center}
\vspace{-.4cm}
\item FME works by eliminating unknowns, each step creates up to \(\frac{n^2}{4}\) new inequalities \(\Rightarrow\) double-exponential complexity
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Related Works \& Applications}
Existing implementations --- all purely sequential
\begin{itemize}
\item Symbolic
	\begin{itemize}
	\item Maple --- very slow, cannot handle large systems
	\item Maxima --- ditto
	\end{itemize}
\item Numeric
	\begin{itemize}
	\item fm-eliminator --- FME very slow, different alg. OK
	\item R --- small systems OK, cannot handle large systems
	\end{itemize}
\end{itemize}
Applications
\begin{itemize}
\item Reordering loop nests for better spatial locality
\item Data dependence analysis --- integer linear programming
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Analysis \& Design I}
%\framesubtitle{Algorithm flowchart}
\makebox[\linewidth]{\parbox{12cm}{
\includegraphics[scale=.61]{figures/flowchart_p}}}
\end{frame}

\begin{frame}
\frametitle{Analysis \& Design II}
%\framesubtitle{Parallelisation}
\begin{itemize}
\item MPI: well-documented, optimised, current industry standard
\item \emph{Template method} design pattern\\
	\begin{itemize}
	\item \texttt{SequentialSolver} calls operations from the \texttt{MPISolver} subclass through overloaded hook methods
	\end{itemize}
\item Main idea --- ensure uniform distribution of inequality categories \& broadcast the smaller set before elimination
\item Inequality equidistribution algorithm
\item Parallel back substitution algorithm
\item Data type independence, handling of \(\pm \infty\)
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Implementation}
\begin{itemize}
\item Type independence through C\textrm{++} templates
	\begin{itemize}
	\item Native built-in \& arbitrary precision (GMP) types
	\end{itemize}
\item Heavy use of C\textrm{++} STL (\texttt{valarray}, \texttt{vector}, algorithms etc.)
\item MPP: a true C\textrm{++} interface to MPI
	\begin{itemize}
	\item Type traits \(\Rightarrow\) \texttt{comm::world(receiver\_rank) {<}{<} eqn;}
	\end{itemize}
\item Exceptions --- all processes need to know
\item Custom logging facilities
\item Miscellanea --- CppUnit, CMake, Doxygen
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Optimisations}
\begin{itemize}
\item C\textrm{++}11 features
	\begin{itemize}
	\item Rvalue references, \texttt{std::move}, \texttt{emplace\_back} etc.
	\end{itemize}
\item Memory allocator: TCMalloc from gperftools
	\begin{itemize}
	\item Efficient small object allocation
	\end{itemize}
\item Broadcasts instead of matching sends/receives
	\begin{itemize}
	\item Improved network utilisation
	\end{itemize}
\center\includegraphics[scale=.6]{figures/bcast}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Evaluation I}
\framesubtitle{Results on 12-processor nodes}
\begin{columns}[c] \hspace{-1.1cm} % the "c" option specifies center vertical alignment
\column{.5\textwidth} % column designated by a command
\includegraphics[scale=.56]{figures/12c_p}
\column{.5\textwidth}
\includegraphics[scale=.56]{figures/12c2_p}
\end{columns}
Data dependent scalability --- \(24\times5\) and \(150\times7\) systems scale well, \(52\times4\) does not without InfiniBand.
\end{frame}

\begin{frame}
\frametitle{Evaluation II}
\framesubtitle{Impact of communication network (4-processor nodes)}
\begin{columns}[c] \hspace{-1.1cm} % the "c" option specifies center vertical alignment
\column{.5\textwidth} % column designated by a command
\includegraphics[scale=.56]{figures/eth_vs_infi_p}
\column{.5\textwidth}
\includegraphics[scale=.56]{figures/eth_vs_infi2_p}
\end{columns}
InfiniBand with all 6 nodes --- large sparse system \(2\times\) more efficient, smaller dense system \(3\times\).
\end{frame}

\begin{frame}
\frametitle{Evaluation III}
\framesubtitle{Comparison with theoretical expectations}
\scriptsize{
Theoretical worst-case scenario\vphantom{q}
\newline

\begin{tabular}{| c | r | r | r | r | r | r | r |}
\hline
\textbf{\textit{i}} & \multicolumn{6}{c|}{\textbf{Inequality counts}} \\ \hline
1 & 10 & 20 & 24 & 30 & 40 & 50 \\ \hline
2 & 25 & 100 & 144 & 225 & 400 & 625 \\ \hline
3 & 156 & 2 500 & 5184 & 12 656 & 40 000 & 97 656 \\ \hline
4 & 6 104 & 1 562 500 & 6 718 464 & $4.0\times10^{7}$ & $4.0\times10^{8}$ & $2.4\times10^{9}$ \\ \hline
5 & 9 313 226 & $6.1\times10^{11}$ & $1.1\times10^{13}$ & $4.0\times10^{14}$ & $4.0\times10^{16}$ & $1.4\times10^{18}$ \\ \hline
6 & $2.2\times10^{13}$ & $9.3\times10^{22}$ & $3.2\times10^{25}$ & $4.0\times10^{28}$ & $4.0\times10^{32}$ & $5.0\times10^{35}$ \\ \hline
7 & $1.1\times10^{26}$ & $2.2\times10^{45}$ & $2.5\times10^{50}$ & $4.0\times10^{56}$ & $4.0\times10^{64}$ & $6.4\times10^{70}$ \\ \hline
\end{tabular}
\newline\newline
%\caption{Inequality counts in the theoretical worst-case scenario}

Actual inequality counts in randomly generated instances
\newline

\begin{tabular}{| c | r | r | r || r | r | r | r | r |}
\hline
\multicolumn{4}{|c||}{\textbf{Dense systems}} & \multicolumn{3}{c|}{\textbf{Sparse systems}} \\ \hline
\textbf{\textit{i}} & \multicolumn{3}{c||}{\textbf{Inequality counts}} & \textbf{\textit{i'}} & \multicolumn{2}{c|}{\textbf{Inequality counts}} \\ \hline
1 & 10 & 20 & 24 & 1 & 150 & 200 \\ \hline
2 & 21 & 44 & 95 & 6 & 623 448 & 562 \\ \hline
3 & 110 & 343 & 894 & 7 & 482 642 693 & 1 041 \\ \hline
4 & 2 625 & 27 423 & 88 253 & 8 & & 28 778 \\ \hline
5 & 1 412 964 & 184 967 954 & 609 292 552 & 9 & & 70 737 891 \\ \hline
\end{tabular}}
%\caption{Actual inequality counts in randomly generated instances}

\end{frame}

\begin{frame}
\frametitle{Conclusion}
\large\begin{itemize}
\item Analysis, design and implementation of a parallel solver
for large systems of linear inequalities ...  {\LARGE\color{green}\checkmark}
\item Various optimisations
	\begin{itemize}
	\item Blitz\textrm{++} matrix library... slowdown {\LARGE\color{red}$\times$}
	\item \texttt{std::slice} with \texttt{std::valarray} ... 15\% speed-up {\LARGE\color{green}\checkmark}
	\item \texttt{std::vector} with \texttt{boost::pool\_allocator} ... slowdown {\LARGE\color{red}$\times$}
	\item gperftools TCMalloc ... 30\% speed-up {\LARGE\color{green}\checkmark}
	\end{itemize}
\item The resulting solver scales reasonably well and achieves good parallel speed-up and efficiency {\LARGE\color{green}\checkmark}
\end{itemize}
%The thesis began with a~detailed mathematical description of the problem of solving large systems of linear inequalities using Fourier--Motzkin elimination. I~thoroughly explained the algorithm and examined its properties and complexity. Furthermore, I~surveyed the strengths and weaknesses of existing implementations of FME. Real world applications of FME were summarised.
%
%After an analysis of each step of FME I~designed a~sequential solver for systems of linear inequalities. I~explored the available platforms and the perils of parallel programming in general and then delved into parallelisation strategies of FME. With emphasis on reusing the functionality present in the sequential solver, I~carefully designed an MPI-based parallel version.
%
%In chapter~\ref{chimpl}, I~dissected important implementation details of both sequential and parallel solvers and gave an explanation of used optimisations. The results discussed in the last chapter are compared to of array accessestheoretical expectations and show that I~succeeded in achieving reasonable speed-up and efficiency with randomly generated data sets in the parallel solver.
%
%Possible future improvements to this work would include designing and implementing an optimised vector representation, capable of both efficient arithmetic operations and of utilising a~custom memory allocation strategy. The memory allocator should take into account that large numbers of tiny vectors are allocated in later phases of FME. Another possible enhancement would be the implementation of type traits for arbitrary precision arithmetic data types, ideally for GNU multi-precision library integer, rational and floating point types.


\end{frame}

\end{document}
