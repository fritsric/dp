#!/bin/sh
cd figures && ./figures.sh && cd ..

pdflatex -synctex=1 -interaction=nonstopmode coverMasters-bw.tex >/dev/null
pdflatex -shell-escape -synctex=1 -interaction=nonstopmode thesis.tex >/dev/null
bibtex thesis.aux >/dev/null
pdflatex -shell-escape -synctex=1 -interaction=nonstopmode thesis.tex >/dev/null
pdflatex -shell-escape -synctex=1 -interaction=nonstopmode thesis.tex >/dev/null

pdflatex -shell-escape -synctex=1 -interaction=nonstopmode presentation.tex >/dev/null
pdflatex -shell-escape -synctex=1 -interaction=nonstopmode presentation.tex >/dev/null
